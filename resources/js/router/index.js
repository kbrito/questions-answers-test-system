import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import store from '../store'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    routes,
    linkActiveClass: 'active'
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(r => r.meta.requiresAuth) && !store.state.token) {
        // window.location = window.Urls.login
        router.push('login')
        return
    }
    next()
})

export default router