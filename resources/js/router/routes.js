import QuestionsPage from '../pages/QuestionsPage.vue'
import QuestionPage from '../pages/QuestionPage.vue'
import MyPostsPage from '../pages/MyPostsPage.vue'
import NotFoundPage from '../pages/NotFoundPage.vue'
import CreateQuestionPage from '../pages/CreateQuestionPage.vue'
import EditQuestionPage from '../pages/EditQuestionPage.vue'
import LoginPage from '../pages/LoginPage.vue'
import LogoutPage from '../pages/LogoutPage.vue'
import RegisterPage from '../pages/RegisterPage.vue'
import PasswordEmailPage from '../pages/PasswordEmailPage.vue'
import PasswordResetPage from '../pages/PasswordResetPage.vue'

const routes = [
    {
        path: '/',
        component: QuestionsPage,
        name: 'home'
    },
    {
        path: '/questions',
        component: QuestionsPage,
        name: 'questions'
    },   
    {
        path: '/questions/create',
        component: CreateQuestionPage,
        name: 'questions.create'
    },
    {
        path: '/questions/:id/edit',
        component: EditQuestionPage,
        name: 'questions.edit'
    },
    {
        path: '/home',
        component: MyPostsPage,
        name: 'my-posts',
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/questions/:slug', 
        component: QuestionPage,
        name: 'questions.show',
        props: true
    },
    {
        path: '/login',
        component: LoginPage,
        name: 'login'
    },
    {
        path: '/logout',
        component: LogoutPage,
        name: 'logout'
    },
    {
        path: '/register',
        component: RegisterPage,
        name: 'register'
    },
    {
        path: '/password/email',
        component: PasswordEmailPage,
        name: 'password.email'
    },
    {
        path: '/password/reset/:token',
        component: PasswordResetPage,
        name: 'password.reset'
    },
    {
        path: '*',
        component: NotFoundPage
    }
]

export default routes