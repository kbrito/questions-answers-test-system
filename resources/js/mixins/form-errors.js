import EventBus from '../event-bus'
export default {
    mounted () {
        EventBus.$on('error', errors => this.errors = errors)
    },
    methods: {
        errorClass (column) {
            return [
                'form-control',
                this.errors[column] && this.errors[column][0] ? 'is-invalid' : ''
            ]
        },
        errorColumn (column) {
            return this.errors[column] && this.errors[column][0] ? true : false
        }
    }
}