import policies from './policies';
import store from '../store';

export default {
    install (Vue, options) {
        Vue.prototype.authorize = function (policy, model) {
            if ( ! store.state.token) return false;
        
            if (typeof policy === 'string' && typeof model === 'object') {
                const user = store.state.user;
        
                return policies[policy](user, model);        
            }
        };

        Vue.prototype.signedIn = store.getters.loggedIn;
    }
}