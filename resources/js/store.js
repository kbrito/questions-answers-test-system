import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    token: localStorage.getItem('access_token') || null,
    user: localStorage.getItem('user') || null,
  },
  getters: {
    loggedIn(state) {
      return state.token !== null
    },
    user(state) {
      return state.user
    }
  },
  mutations: {
    retrieveToken(state, data) {
      state.token = data.token
      state.user = data.user
    },
    destroyToken(state) {
      state.token = null
      state.user = null
    },
    setUser(state, data) {
      state.user = data
    }
  },
  actions: {
    retrieveToken(context, credentials) {

      return new Promise((resolve, reject) => {
        axios.post('/login', {
          email: credentials.email,
          password: credentials.password,
        })
          .then(response => {
            //console.log(response)
            const token = response.data.token
            const user = response.data.user
            localStorage.setItem('access_token', token)
            localStorage.setItem('user', user)
            context.commit('retrieveToken', response.data)
            // console.log(response.data)
            resolve(response)
          })
          .catch(error => {
            //console.log(error)
            reject(error)
          })
      })

    },
    destroyToken(context) {
      
      if (context.getters.loggedIn){
        
        return new Promise((resolve, reject) => {
            console.log(context.state.token)
            axios.delete('logout', {
              headers: { Authorization: "Bearer " + context.state.token }
            })
            .then(response => {
              //console.log(response)
              localStorage.removeItem('access_token')
              localStorage.removeItem('user')
              context.commit('destroyToken')
  
              resolve(response)
            })
            .catch(error => {
              //console.log(error)
              localStorage.removeItem('access_token')
              localStorage.removeItem('user')
              context.commit('destroyToken')

              reject(error)
            })
        })

      }
    },
    getUser(context) {
      return new Promise((resolve, reject) => {
        axios.get('/user',{
          headers: { Authorization: "Bearer " + context.state.token }
        }).
          then(response => {
              const user = response.data
              localStorage.setItem('user', user)
              context.commit('setUser', user)
              resolve(response)
          }).
          catch(error => {
            reject(error)
          })
      })
    }
  }
})

export default store