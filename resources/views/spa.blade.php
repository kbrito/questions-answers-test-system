<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/prismjs-themes/prism.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <router-link class="navbar-brand" :to="{ name: 'home' }">Laravel Q/A</router-link>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <router-link class="nav-item" tag="li" :to="{ name: 'questions' }"><a class="nav-link">Questions</a></router-link>
                        <router-link class="nav-item" tag="li" :to="{ name: 'my-posts' }"><a class="nav-link">My Posts</a></router-link>
                        <!-- Authentication Links -->
                        {{-- @guest --}}
                            <router-link  v-if="!auth" class="nav-item" tag="li" :to="{ name: 'login' }"><a class="nav-link">Login</a></router-link>
                            <router-link  v-if="!auth" class="nav-item" tag="li" :to="{ name: 'register' }"><a class="nav-link">Register</a></router-link>
                            
                        {{-- @else --}}
                            <li class="nav-item dropdown" v-if="auth">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    User <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <router-link class="dropdown-item" :to="{ name :'logout' }">
                                        Logout
                                    </router-link>
                                </div>
                            </li>
                        {{-- @endguest --}}
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <transition name="fade" mode="out-in">
                <router-view></router-view>
            </transition>
        </main>
    </div>
    <!-- Scripts -->
    <script>
        window.Auth = @json([
            'signedIn' => Auth::check(),
            'user' => Auth::user()
        ]);
        window.Urls = @json([
            'api' => url('/api'),
            'login' => route('login')
        ]);
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>