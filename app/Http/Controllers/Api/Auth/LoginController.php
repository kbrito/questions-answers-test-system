<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string',
        ]);

        $user = User::where('email', $request->email)->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        return [
            'user' => $user,
            'token' => $user->createToken('laravel-qa')->plainTextToken
        ];

        // return $user->createToken('laravel-qa')->plainTextToken;

        // $request->request->add([
        //     'grant_type' => 'password',
        //     'client_id' => config('services.passport.client_id'),
        //     'client_secret' => config('services.passport.client_secret'),
        //     'username' => $request->username,
        //     'password' => $request->password,
        // ]);
 
        // $tokenRequest = Request::create(env('APP_URL') . '/oauth/token', 'post');
 
        // $response = Route::dispatch($tokenRequest);
 
        // return $response;


    }

    public function destroy(Request $request)
    {
        // $request->user()->token()->revoke();

        // Revoke all tokens...
        $request->user()->tokens()->delete();

        // Revoke the user's current token...
        // $request->user()->currentAccessToken()->delete();

        return response()->noContent();
    }
}
